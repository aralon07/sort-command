#include <stdio.h>
#include <string.h>
#include "sort_funcn.h"


int sort_i(char *string1, char *string2) {
	char *str1 = string1, *str2 = string2;
	while(*str1 != '\0' && *str2 != '\0') {
		if(*str1 >= 32 && *str1 <= 127 && *str2 >= 32 && *str2 <= 127) {
			if(*str1 != *str2)
				return strcmp_u(str1, str2);
		}
		else if(*str1 >= 32 && *str1 <= 127)
			++str2;
		else if(*str2 >= 32 && *str2 <= 127)
			++str1;
	}
	if(*str1 == '\0' && *str2 == '\0')
		return strcmp_u(string1, string2);
	else if(*str1 == '\0')
		return -1;
	else if(*str2 == '\0')
		return 1;
	return 0;
}

int strcmp_i_qsort(const void *void_str1, const void *void_str2) {
    char **str1 = (char **)void_str1;
    char **str2 = (char **)void_str2;
    return sort_i(*str1, *str2);
}