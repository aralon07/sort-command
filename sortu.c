#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sort_funcn.h"

void strlwr(char *str) {
    int i = 0;
    for(i = 0; i < strlen(str); i++)
        if((str)[i] >= 'A' && (str)[i] <= 'Z')
            (str)[i] += 32;
}

int strcmp_u_qsort(const void *void_str1, const void *void_str2) {
    char **str1 = (char **)void_str1;
    char **str2 = (char **)void_str2;
    return strcmp_u(*str1, *str2);
}

int strcmp_u(char *str1, char *str2) {
    // printf("IN STR U\n");
    char string1[5000];
    strcpy(string1, str1);
    char string2[5000];
    strcpy(string2, str2);
    strlwr(string1);
    strlwr(string2);
    int value = mystrcmp(string1, string2);
    if(value != 0)
        return value;
    return (mystrcmp(str1, str2) > 0) ? -1 : 1;
}


//
/*
int strcmp_u(char *str1, char *str2) {
    int str1_small = 0, str2_small = 0;
    while(*str1 != '\0' && *str2 != '\0') {
        if(*str1 >= 'a' && *str1 <= 'z')
            str1_small = 1;
        else if(*str1 >= 'A' && *str1 <= 'Z')
            str1_small = 2;
        else
            str1_small = 3;
        if(*str2 >= 'a' && *str2 <= 'z')
            str2_small = 1;
        else if(*str2 >= 'A' && *str2 <= 'Z')
            str2_small = 2;
        else
            str2_small = 3;
        if((str1_small == 1 && str2_small == 1) || (str1_small == 1 && str2_small == 3))
            return *str1 - *str2;
        else if(str1_small == 1 && str2_small == 2) {
            if(*str1 - 32 == *str2)
                return -1;
            else if(*str1 - 32 > *str2)
                return 1;
            else
                return -1;
        } else if(str1_small == 2 && str2_small == 1) {
            if(*str1 == *str2 - 32)
                return 1;
            else if(*str1 > *str2 - 32)
                return 1;
            else
                return -1;
        } else if((str1_small == 2 && str2_small == 2) || (str1_small == 2 && str2_small == 3))
            return *str1 - *str2;
        else
            return *str1 - *str2;
    }
}
*/
