#include "sort_funcn.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <math.h>

int strcmp_k(char *string1, char *string2, float f ) {
    // printf("IN STR K\n");
    int col_num = floor(f);
    int char_num = (f - col_num) * 10;
    char *str1 = string1, *str2 = string2;
    int i = 0, j = 0, len1 = strlen(str1), len2 = strlen(str2), temp = 0;
    while(*str1 == ' ')
        ++str1;
    while(temp < col_num) {
        if(*str1 == '\0')
            break;
        if(*str1 == ' ')
            ++temp;
        ++str1;
    }
    temp = 0;
    while(*str2 == ' ')
        ++str2;
    while(temp < col_num) {
        if(*str2 == '\0')
            break;
        if(*str2 == ' ')
            ++temp;
        ++str2;
    }
    if(*str1 == '\0' && *str2 == '\0') {
        return strcmp_u(string1, string2);
    }
    else if(*str1 == '\0') {
        return -1;
    }
    else if(*str2 == '\0') {
        return 1;
    }
    return strcmp_u(str1, str2);
}
