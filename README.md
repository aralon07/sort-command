												

												IMPLEMENTATION OF COMMAND LINE SORT

															NAME - RITESH
															 S.Y CE , S3	
															MIS -111603066





This is a c program to implement command line sort.
It includes the following options:
 -k -n -b  -d  -i  -r  -m  -t -u 
It can sort files of any size (e.g. 10G, 100GB, etc.),  by means of external sort.
External sort is not incorporated in the main code though, however it works individually.(external_sort.c works)
All the commands and instructions on how to use it can be understood through the snapshots provided in the same folder.
sort -k work on the basis of 0 column, i.e column 1 of the file to be sorted is taken as input column 0 in sortk.c.
to be executed using the command :- make, followed by, ./project.
sort -i is also written individually and I was not able to incorporate it into the main code.
