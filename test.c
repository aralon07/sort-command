#include <stdio.h>
#include "sort_funcn.h"

int main(int argc, char const *argv[])
{
	FILE *fp1 = fopen(argv[1], "r"), *fp2 = fopen(argv[2], "r");
	sort_merge(fp1, fp2);
	fclose(fp1);
	fclose(fp2);
	return 0;
}