#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *fp = fopen(argv[1], "r");
	if(!fp) {
		return -1;
	}
	split_files(fp);
	printf("SPLITTING DONE \n");
	ext_sort();
	return 0;
}