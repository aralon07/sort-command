#include <stdio.h>


#include <sys/types.h> /* for open() */
#include <sys/stat.h> /* for open() */
#include <fcntl.h> /* for open() */
#include <stdlib.h> /* for exit() */
#include <unistd.h> /* for read() */
#include <errno.h> /* for using errno and perror */
#include <string.h>
#include <limits.h>
#include "sort_funcn.h"

//NUMERIC VALUE OF STRING
int str_to_int(char *s) {

    int num = 0;
    int i;
    for (i = 0; s[i]!= '\0'; i++) {
        if(s[i]< '0' || s[i]>'9')
            return -1;
        num = num * 10 + (s[i] - '0');
    }
    return num;
}

int main(int argc, char* argv[]){

    FILE* fp;
    char str[5000];
    int i ,j, countr = 0, *toprint, len = 0;
    int is_u = 0, is_k = 0, is_n = 0, is_r = 0, is_b = 0, is_d = 0, is_i = 0, is_merge = 0;
    int repeat = 1;
    float col = 0;

    if(argc < 2) {
        printf("usage: ./a.out <filename> [options]\n");
        return EINVAL;
    }

    for(i = 1; i < argc; i++) {
        if(strcmp(argv[i], "-m") == 0) {
            if(argc < 4) {
                printf("INVALID SYNTAX\n");
                return EINVAL;
            }
            FILE *fp1 = fopen(argv[2], "r");
            if(fp1 == NULL) {
                printf("Usage : ./project -m <filename1> <filename2>\n");
                return -1;
            }
            FILE *fp2 = fopen(argv[3], "r");
            if(fp2 == NULL) {
                printf("Usage : ./project -m <filename1> <filename2>\n");
                return -1;
            }
            merge_sort(fp1, fp2);
            fclose(fp1);
            fclose(fp2);
            printf("\n");
            return 2;
        }
    }

    fp = fopen(argv[1], "r");

    if(fp == NULL) {
      perror("Error opening file");
      return(-1);
   }
   // -----------------------------------------------------------------------------
    //Handle following options: -k -n -b  -d  -i  -r  -m  -t -u
    for(i = 2; i < argc ; i++){
        if(strcmp(argv[i], "-k") == 0){
            is_k = 1;
            if(i+1 >= argc ) {
                printf("Specify column number\n");
                return EINVAL;
            }
            col = atof(argv[i+1]);
            // if(col == -1)
            //     return EINVAL;
        }
        if(strcmp(argv[i], "-u") == 0){
            is_u = 1;
        }
        if(strcmp(argv[i], "-r") == 0) {
            is_r = 1;
        }
        if(strcmp(argv[i], "-n") == 0) {
            is_n = 1;
        }
        if(strcmp(argv[i], "-b") == 0) {
            is_b = 1;
        }
        if(strcmp(argv[i], "-d") == 0) {
            is_d = 1;
        }
        if(strcmp(argv[i], "-i") == 0) {
            is_i = 1;
        }

    }

   // -----------------------------------------------------------------------------

    while(fgets(str,5000, fp)!= NULL){
        countr++;
    }
    toprint = (int *) calloc(sizeof(int), countr);
    fseek(fp, 0, SEEK_SET);

    char **arr = (char **) malloc(countr * sizeof(char *));

    for(i = 0; i < countr; i++) {
        if (fgets(str, 5000, fp) == 0)
            break;
        arr[i] = (char *)malloc(sizeof(char) * strlen(str) + 1);
        str[strlen(str) - 1] = '\0';
        strcpy(arr[i], str);

        // arr[strlen(str)] = '\0';
    }
    char *temp;
    int strcmp_value = 0;
    if(argc  == 2){
        qsort(arr, countr, sizeof(char *), strcmp_u_qsort);
    } else if(is_i || is_d || is_n || is_b) {
        qsort(arr, countr, sizeof(char *), strcmp_u_qsort);
    }else if(is_u){
        qsort(arr, countr, sizeof(char *), strcmp_u_qsort);
        j = 0;
        for(i = 1; i < countr; i++) {
            if(strcmp(arr[j], arr[i]) == 0) {
                toprint[i] = 1;
            }
            else
                j = i;
        }
    } else if(is_k) {
        for (int i = 0; i < countr - 1; i++) {
            for(j = 0; j < countr - i - 1; j++) {
                strcmp_value = strcmp_k(arr[j], arr[j + 1], col);
                if(strcmp_value > 0) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    } else {
        qsort(arr, countr, sizeof(char *), strcmp_u_qsort);
    }
    if(is_r == 0) {
        if(is_u){
            for(i = 0; i < countr; i++) {
                if(toprint[i] == 0)
                    printf("%s\n", arr[i]);
                // printf("\n");
            }
        } else {
            for(i = 0; i < countr; i++) {
                printf("%s\n", arr[i]);
                // printf("\n");
            }
        }
    } else if(is_r == 1) {
        if(is_u) {
            for (i = countr - 1; i >= 0; i--){
                if(toprint[i] == 0) {
                    printf("%s", arr[i]);
                    printf("\n");
                }
            }
        } else {
            for (i = countr - 1; i >= 0; i--){
                printf("%s", arr[i]);
                printf("\n");
            }
        }
    }
    free(arr);
    fclose(fp);

    return 0;

}

// int strcmp_n(char *str1, char *str2) {
//     int len1 = strlen(str1), len2 = strlen(str2), i = 0, j = 0;
//     while(i < len1 && j < len2) {
//         if((str1[i] >= '0' && str1[i] <= '9') && (str2[j] >= '0' && str2[j] <= '9')) {
//             if(str1[i] != str2[j])
//                 return str1[i] - str2[j];
//             ++i;
//             ++j;
//         } else if(!(str1[i] >= '0' && str1[i] <= '9') && !(str2[j] >= '0' && str2[j] <= '9')) {
//             ++i;
//             ++j;
//         } else if(!(str1[i] >= '0' && str1[i] <= '9'))
//             i++;
//         else if(!(str2[j] >= '0' && str2[j] <= '9'))
//             ++j;
//     }
//     if(i == len1 && j == len2)
//         return 0;
//     else if(i == len1 && j != len2)
//         return -1;
//     else if(i != len1 && j == len2)
//         return 1;
//     return 0;
// }




//Handle following options: -k -n -b  -d  -i  -r  -m  -t -u
