#include "sort_funcn.h"
#include <string.h>
#include <limits.h>
//COMPARING STRINGS
int mystrcmp(char *str1, char *str2){
    int i = 0, j = 0, len1 = strlen(str1), len2 = strlen(str2);
    int is_out_big = 0, is_out_small = 0, is_out_number = 0;
    // printf("SHTI\n");
    while(i < len1 && j < len2) {
        if((str1[i] < '0' || str1[i] > '9') && (str1[i] < 'a' || str1[i] > 'z') && (str1[i] < 'A' || str1[i] > 'Z')) {
            i++;
            continue;
        }
        if((str2[j] < '0' || str2[j] > '9') && (str2[j] < 'a' || str2[j] > 'z') && (str2[j] < 'A' || str2[j] > 'Z')) {
            j++;
            continue;
        }
        if(str1[i] != str2[j])
            return strcmp(str1 + i, str2 + j);
        i++;
        j++;
    }
    /*
    while(i < len1 && j < len2) {
        if(str1[i] < 'a' || str1[i] > 'z') {
            is_out_small = 1;
            // printf("NOT SMALL1\n");
        }
        if(str1[i] < 'A' || str1[i] > 'Z') {
            is_out_big = 1;
            // printf("NOT BIG1\n");
        }
        if(str1[i] < '0' || str1[i] > '9') {
            is_out_number = 1;
            // printf("NOT NUMBER1\n");
        }

        if(!is_out_small && !is_out_number && !is_out_big) {
            // printf("SHIT1\n");            
            i++;
            continue;
        }
        is_out_big = is_out_number = is_out_small = 0;

        if(str2[j] < 'a' || str2[j] > 'z') {
            is_out_small = 1;
            // printf("NOT SMALL2\n");
        }
        if(str2[j] < 'A' || str2[j] > 'Z') {
            is_out_big = 1;
            // printf("NOT BIG2\n");
        }
        if(str2[j] < '0' || str2[j] > '9') {
            is_out_number = 1;
            // printf("NOT NUMBER2\n");
        }
    
        if(!is_out_small && !is_out_number && !is_out_big) {
            // printf("SHIT2\n");
            j++;
            continue;
        }

        if(str1[i] != str2[j])
            return strcmp_try(str1 + i, str2 + j);
        i++;
        j++;
    }
    */   
    if(i == len1 && j == len2)
        return 0;
    else if(i == len1)
        return 1;
    else if(j == len2)
        return -1;
    return 0;
}
