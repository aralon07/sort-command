#include <stdio.h>

void merge_sort(FILE *fp1, FILE *fp2);
int mystrcmp(char *str1, char *str2);
int strcmp_k(char *str1, char *str2, float k);
int strcmp_u(char *str1, char *str2);
int strcmp_u_qsort(const void *void_str1, const void *void_str2);