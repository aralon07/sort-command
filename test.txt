

Following commands have been found to be working properly on my sort program :-

./project datafile1.txt
./project -k datafile1.txt
./project -u datafile1.txt
./project -i datafile1.txt
./project -m datafile2.txt datafile3.txt
./project -n datafile1.txt
./project -b datafile1.txt 
./project -n datafile1.txt
./project -d datafile1.txt

[datafile2.txt and datafile3.txt are already sorted, so those two are only used in merge sort]
