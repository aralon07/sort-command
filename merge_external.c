#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cstring_cmp(const void *a, const void *b)
{
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
}


int sort_single_file(FILE *fp) {
    // FILE *fp = fopen(name, "r+");
    int countr = 0, i = 0, j = 0;
    char str[5000], **arr, *temp;
    while(fgets(str, 5000, fp) != NULL)
        ++countr;
    arr = (char **) malloc(sizeof(char *) * countr);
    fseek(fp, 0, SEEK_SET);
    for(i = 0; i < countr; i++) {
        if(fgets(str, 5000, fp) == NULL)
            break;
        arr[i] = (char *) malloc(sizeof(char) * strlen(str) + 1);
        strcpy(arr[i], str);
    }
    qsort(arr, countr, sizeof(char *), cstring_cmp);
    rewind(fp);
    for(i = 0; i < countr; i++)
        fputs(arr[i], fp);

    for(i = 0; i < countr; i++)
        free(arr[i]);
    free(arr);

    return 0;
}

void sort_all_files() {
// int main(int argc, char const *argv[]) {
    FILE *fp;
    int i = 0;
    char arr[500] = "0.data";
    while((fp = fopen(arr, "r+")) != NULL) {
        // printf("INERER\n")
        printf("SORTING %d\n", i);
        sort_single_file(fp);
        i++;
        sprintf(arr, "%d.data", i);
        fclose(fp);
    }
}

// int main() {
void ext_sort() {
    char filename[500] = "0.data";
    FILE *input, *output, *file;
    int i = 1, j = 0, counter = 0;
    char str1[5000], str2[5000];
    printf("SORITN STRATED\n");
    sort_all_files();
    printf("SORTING ENDED\n");
    while((file = fopen(filename, "r")) != NULL) {
        if(i == 1) {
            input = fopen("1", "r+");
            if(input == NULL) {
                input = fopen("1", "w");
                fclose(input);
                input = fopen("1", "r");
            }
            output = fopen("-1", "w+");
        } else {
            input = fopen("-1", "r+");
            output = fopen("1", "w+");
        }
        if(fgets(str1, 5000, input) == NULL) {
            while(fgets(str2, 5000, file) != NULL)
                fputs(str2, output);
        }
        else if(fgets(str2, 5000, file) == NULL) {
            while(fgets(str1, 5000, file) != NULL)
                fputs(str1, output);
        } else {
            while(1) {
                if(strcmp(str1, str2) > 0) {
                    fputs(str2, output);
                    if(fgets(str2, 5000, file) == NULL)
                        break;
                } else {
                    fputs(str1, output);
                    if(fgets(str1, 5000, input) == NULL)
                        break;
                }
            }
            if(fgets(str1, 5000, file) == NULL) {
                while(fgets(str2, 5000, input) != NULL)
                    fputs(str2, output);
            } else if(fgets(str1, 5000, input) == NULL) {
                while(fgets(str2, 5000, file) != NULL)
                    fputs(str2, output);
            }
        }
        fclose(file);
        fclose(input);
        fclose(output);
        remove(filename);
        i *= -1;
        j++;
        sprintf(filename, "%d.data", j);
    }
    if(j % 2 == 0) {
        remove("-1");
        rename("1", "output");
    }
    else {
        remove("1");
        rename("-1", "output");
    }
}
