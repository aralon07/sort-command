#include "sort_funcn.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// SPLITS A BIG FILE TO SMALLER FILE
int split_files(FILE *file) {
	int i = 0, size = 25, lines = 0;
	FILE *fp;
	char str[50000], name[5000];
	fp = fopen("0.data", "w+");
	while(fgets(str, 50000, file) != NULL) {
		if(lines > 1000000l) {
			printf("%d Done\n", i);
			lines = 0;
			fclose(fp);
			i++;
			sprintf(name, "%d.data", i);
			fp = fopen(name, "w+");
		}
		fputs(str, fp);
		lines++;
	}
	fclose(fp);
	return 0;
}